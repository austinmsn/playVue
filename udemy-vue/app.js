Vue.createApp({
    data(){
        return {
            goals: ['a', 'b', 'c'],
            enteredValue: ''
        };
    },
    methods:{
        addGoal(){
            console.log('addGoal');
            this.goals.push(this.enteredValue);
        }
    }
});