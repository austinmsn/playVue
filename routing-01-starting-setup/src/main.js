import { createApp } from 'vue';
import { createRouter, createWebHistory } from 'vue-router';

import App from './App.vue';
import TeamsList from './components/teams/TeamsList.vue';
import UsersList from './components/users/UsersList.vue';
import TeamMembers from './components/teams/TeamMembers.vue';

const router = createRouter({
    history: createWebHistory(),
    routes: [
        {path: '/teams', component: TeamsList},    //out-doman.com/teams
        {path: '/teams/:teamId', component: TeamMembers},    
        {path: '/teams/new', component: TeamsList},    
        {path: '/users', component: UsersList},    
    ],
    linkActiveClass: 'router-link-active'
});
const app = createApp(App);

app.use(router);

app.mount('#app');
